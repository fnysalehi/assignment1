#include <string>
#include <iostream>
#include <io.h>
#include <fcntl.h>
#include "FilePrinter.hpp"
#include "Buffer.hpp"

int main()
{

    FilePrinter filePrinter;
    Buffer<char> rawData = filePrinter.loadFile("myFileUTF8.txt");
    Buffer<int> convertedData = filePrinter.convertUTF8ToUnicode(rawData);
    filePrinter.printUnicode(convertedData);
    bufferFree(rawData);
    bufferFree(convertedData);
    return 0;
}

