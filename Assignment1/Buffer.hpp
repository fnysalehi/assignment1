#pragma once
#include <fstream>

template <typename T>
struct Buffer {
    T* ptr = nullptr;
    size_t size = 0;
};

template <typename T>
Buffer<T> bufferInit(const size_t& size) {
    Buffer<T> buffer = {};
    buffer.ptr = new T[size];
    buffer.size = size;
    return buffer;
}

template <typename T>
void bufferFree(Buffer<T> const& buffer) {
    delete[] buffer.ptr;
    T* ptr = nullptr;
    size_t size = 0;
}