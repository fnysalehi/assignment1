#pragma once

#include "Buffer.hpp"
#include <string>


class FilePrinter {
private:
    size_t countUTF8Chars(Buffer<char> const& buffer);
public:
    FilePrinter();
    Buffer<char> loadFile(char const * filePath);
    void printAscii(Buffer<char> const& buffer);
    Buffer<int> convertUTF8ToUnicode(Buffer<char> const& buffer);
    void printUnicode(Buffer<int> const & buffer);
};

