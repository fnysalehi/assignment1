#include "FilePrinter.hpp"
#include <string>
#include <iostream>
#include <fstream>

FilePrinter::FilePrinter() {
    
}

Buffer<char> FilePrinter::loadFile(char const * filePath){
    Buffer<char> buffer = {};
    
    if (filePath) {
        std::ifstream file(filePath, std::ios::binary);

        if (file) {
            file.seekg(0, std::ios::end);
            const size_t size = static_cast<size_t>(file.tellg());
            buffer = bufferInit<char>(size);
            file.seekg(0, std::ios::beg);
            file.read(buffer.ptr, buffer.size);
        }

    }
    return buffer;

}

size_t FilePrinter::countUTF8Chars(Buffer<char> const& buffer) {
    size_t count = 0;
    for (size_t index = 0; index < buffer.size; ++index) {
        
        if (((buffer.ptr[index] & 240) ==240)    &&                //4 bytes
           ((buffer.ptr[index + 1] & 128) ==128) &&
           ((buffer.ptr[index + 2] & 128) == 128) &&
           ((buffer.ptr[index + 3] & 128) == 128)) {
                index += 3;
        }
        else if (((buffer.ptr[index] & 224) ==224) &&           //3 bytes
            ((buffer.ptr[index + 1] & 128) == 128) &&
            ((buffer.ptr[index + 2] & 128) == 128)) {
            index += 2;
        }
        else if (((buffer.ptr[index] & 192) == 192) &&            //2 bytes
            ((buffer.ptr[index + 1] & 128) == 128)) {
            index++;
        }
        else if ((buffer.ptr[index] & 128) == 128) {             //wrong data    
            //decide what to do
            //for now, just ignore that and skip to correct data
            count--;
        }
        else {                                          //1 byte
            //Do Nothing
        }

        count++;
    }
    return count;
}

void FilePrinter::printAscii(Buffer<char> const & buffer) {
    int line = 0, column = 0;
    
    if (buffer.ptr) {
        auto unsBuffer = reinterpret_cast<unsigned char*>(buffer.ptr);
        for (size_t index = 0; index < buffer.size; ++index) {
            
            if(static_cast<int>(unsBuffer[index]) == 13 || static_cast<int>(unsBuffer[index]) == 10) {
                std::cout << "ASCII Control Character\t\tASCII Code: " << static_cast<int>(unsBuffer[index]) << "\t\t\t\t\tLine: " << line << "\t\tColumn: " << column << std::endl;
                
                if (!(static_cast<int>(unsBuffer[index]) == 13 && static_cast<int>(unsBuffer[index + 1]) == 10)) {
                    line++;
                    column = -1;
                }

            }
            else if (static_cast<int>(unsBuffer[index]) < 32) {

                std::cout << "ASCII Control Character\t\tASCII Code: "  << static_cast<int>(unsBuffer[index]) << "\t\t\t\t\tLine: " << line << "\t\tColumn: " << column << std::endl;
            }
            else if (static_cast<int>(unsBuffer[index]) > 126) {
                std::cout << "ASCII Extended Character\tASCII Code: " << static_cast<int>(unsBuffer[index]) << "\t\t\t\t\tLine: " << line << "\t\tColumn: " << column << std::endl;
            }
            else {
                std::cout << "Printable ASCII character\tASCII Code: " << static_cast<int>(unsBuffer[index]) << "\t\tCharacter: " << unsBuffer[index] << "\t\tLine: " << line << "\t\tColumn: " << column << std::endl;
            }
            
            column++;
        }
    }
}

Buffer<int> FilePrinter::convertUTF8ToUnicode(Buffer<char> const & buffer) {
    Buffer<int> intBuffer = {};

    if (buffer.ptr) {
        auto unsBuffer = reinterpret_cast<unsigned char*>(buffer.ptr);
        size_t size = countUTF8Chars(buffer);
        intBuffer = bufferInit<int>(size);
        int count = 0;

        for (size_t index = 0; index < buffer.size; ++index) {

            if (((unsBuffer[index] & 240) == 240 )     &&                //4 bytes
                ((unsBuffer[index + 1] & 128) == 128) &&
                ((unsBuffer[index + 2] & 128) == 128) &&
                ((unsBuffer[index + 3] & 128) == 128)) {
                intBuffer.ptr[count] = (static_cast<int>(unsBuffer[index] & 7)      << 18) +
                                       (static_cast<int>(unsBuffer[index + 1] & 63) << 12) +
                                       (static_cast<int>(unsBuffer[index + 2] & 63) << 6)  +
                                       (static_cast<int>(unsBuffer[index + 3] & 63));
                index += 3;
            }
            else if (((unsBuffer[index] & 224) == 224)    &&           //3 bytes
                    ((unsBuffer[index + 1] & 128) == 128) &&
                    ((unsBuffer[index + 2] & 128) == 128)) {
                intBuffer.ptr[count] = (static_cast<int>(unsBuffer[index] & 15)     << 12) +
                                       (static_cast<int>(unsBuffer[index + 1] & 63) << 6)  +
                                       (static_cast<int>(unsBuffer[index + 2] & 63));
                index += 2;
            }
            else if (((unsBuffer[index] & 192) == 192)    &&            //2 bytes
                    ((unsBuffer[index + 1] & 128) == 128)) {

                intBuffer.ptr[count] = (static_cast<int>(unsBuffer[index] & 31) << 6) +
                                       (static_cast<int>(unsBuffer[index + 1] & 63));
                index++;
            }
            else if ((unsBuffer[index] & 128) == 128) {             //wrong data    
                //decide what to do
                //for now, just ignore that and skip to correct data
                count--;

            }
            else {                                          //1 byte
                intBuffer.ptr[count] = static_cast<int>(unsBuffer[index]);
            }
            
            count++;
        }
    }
    return intBuffer;
}

void FilePrinter::printUnicode(Buffer<int> const & buffer) {
    int line = 0, column = 0;

    if (buffer.ptr) {
        for (size_t index = 0; index < buffer.size; ++index) {

            if (buffer.ptr[index] == 13 || buffer.ptr[index] == 10) {
                std::cout << "ASCII Control Character\t\tASCII Code: " << buffer.ptr[index] << "\t\t\t\t\tLine: " << line << "\t\tColumn: " << column << std::endl;

                if (!(buffer.ptr[index] == 13 && buffer.ptr[index+1] == 10)) {
                    line++;
                    column = -1;
                }

            }
            else if (buffer.ptr[index] < 32) {

                std::cout << "ASCII Control Character\t\tASCII Code: " << buffer.ptr[index] << "\t\t\t\t\tLine: " << line << "\t\tColumn: " << column << std::endl;
            }
            else if (buffer.ptr[index] > 126) {
                std::cout << "ASCII Extended Character\tASCII Code: " << buffer.ptr[index] << "\t\t\t\t\tLine: " << line << "\t\tColumn: " << column << std::endl;
            }
            else {
                std::cout << "Printable ASCII character\tASCII Code: " << buffer.ptr[index] << "\t\tCharacter: " << static_cast<char>(buffer.ptr[index]) << "\t\tLine: " << line << "\t\tColumn: " << column << std::endl;
            }

            column++;
        }
    }
}